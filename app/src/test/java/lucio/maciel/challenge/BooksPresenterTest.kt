package lucio.maciel.challenge

import com.nhaarman.mockito_kotlin.*
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import lucio.maciel.challenge.base.UiModelMapper
import lucio.maciel.challenge.books.infrastructure.BooksRepository
import lucio.maciel.challenge.books.infrastructure.model.Book
import lucio.maciel.challenge.books.presentation.BooksPresenter
import lucio.maciel.challenge.books.presentation.BooksView
import lucio.maciel.challenge.books.presentation.model.BookUiModel
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import kotlin.coroutines.experimental.EmptyCoroutineContext
import org.hamcrest.CoreMatchers.`is` as isEqualTo

class BooksPresenterTest {

    private lateinit var sut: BooksPresenter

    @Mock
    private lateinit var view: BooksView
    @Mock
    private lateinit var mapper: UiModelMapper<Book, BookUiModel>
    @Mock
    private lateinit var repository: BooksRepository

    private val book = Book("title", "author", "thumbnail")
    private val books = listOf(book)
    private val bookUiModel = BookUiModel("title", "thumbnail", "author")

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(mapper.map(book)).thenReturn(bookUiModel)

        sut = BooksPresenter(repository, mapper, EmptyCoroutineContext)
        sut.attach(view)
    }

    @Test
    fun `loadBooks() should call showLoading, showBooks and hideLoading`() {
        runBlocking {
            Mockito.`when`(repository.loadBooks()).thenReturn(books)
            sut.loadBooks()
        }

        verify(view).showLoading()
        verify(view).showBooks(check {
            MatcherAssert.assertThat(it.size, isEqualTo(1))
            with(it[0]) {
                MatcherAssert.assertThat(title, isEqualTo<CharSequence>("title"))
                MatcherAssert.assertThat(author, isEqualTo<CharSequence>("author"))
                MatcherAssert.assertThat(thumbnail, isEqualTo("thumbnail"))
            }
        })
        verify(view).hideLoading()

        verifyNoMoreInteractions(view)
    }

    @Test
    fun `loadBooks() should call showLoading, showError and hideLoading on errors`() {
        runBlocking {
            doThrow(RuntimeException("Error getting books")).`when`(repository).loadBooks()
            sut.loadBooks()
        }

        runBlocking {
            // TODO - Investigate why need to use a delay here. Probably because the Exception
            // occurs on CommonPool context. A better idea would be inject both the UI and CommonPool
            // context on the presenter, so we could run it blocking altogether
            delay(200)
            verify(view).showLoading()
            verify(view).showError()
            verify(view).hideLoading()

            verifyNoMoreInteractions(view)
        }
    }
}