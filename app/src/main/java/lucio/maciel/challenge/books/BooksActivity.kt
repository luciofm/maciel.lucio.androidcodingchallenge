package lucio.maciel.challenge.books

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_books.*
import kotlinx.coroutines.experimental.android.UI
import lucio.maciel.challenge.R
import lucio.maciel.challenge.books.infrastructure.NetworkBooksRepository
import lucio.maciel.challenge.books.presentation.BookUiModelMapper
import lucio.maciel.challenge.books.presentation.BooksPresenter
import lucio.maciel.challenge.books.presentation.BooksView
import lucio.maciel.challenge.books.presentation.model.BookUiModel

class BooksActivity : AppCompatActivity(), BooksView {

    // TODO - use dependency injection
    private val repository = NetworkBooksRepository()
    private val mapper = BookUiModelMapper()
    private val presenter = BooksPresenter(repository, mapper, UI)

    private val adapter = BooksAdapter()
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_books)

        presenter.attach(this)

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapter

        loadBooks()
    }
    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }

    private fun loadBooks() {
        snackbar?.dismiss()
        presenter.loadBooks()
    }

    override fun showLoading() {
        loading_bar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading_bar.visibility = View.GONE
    }

    override fun showBooks(books: List<BookUiModel>) {
        adapter.add(books)
    }

    override fun showError() {
        snackbar?.dismiss()
        snackbar = Snackbar.make(recycler_view, R.string.loading_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry) {
                    loadBooks()
                }.apply {
                    show()
                }
    }
}
