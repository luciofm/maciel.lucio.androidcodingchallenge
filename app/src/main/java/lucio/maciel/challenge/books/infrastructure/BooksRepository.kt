package lucio.maciel.challenge.books.infrastructure

import lucio.maciel.challenge.books.infrastructure.model.Book

interface BooksRepository {
    suspend fun loadBooks(): List<Book>
}