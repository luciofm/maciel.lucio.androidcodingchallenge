package lucio.maciel.challenge.books.presentation

import lucio.maciel.challenge.books.presentation.model.BookUiModel

interface BooksView {
    fun showLoading()

    fun hideLoading()

    fun showBooks(books: List<BookUiModel>)

    fun showError()
}