package lucio.maciel.challenge.books.infrastructure.model

data class Book(
    val title: String,
    val author: String?,
    val thumbnail: String?
)