package lucio.maciel.challenge.books.presentation.model

data class BookUiModel(
    val title: CharSequence,
    val thumbnail: String?,
    val author: CharSequence?
)