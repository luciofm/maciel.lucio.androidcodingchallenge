package lucio.maciel.challenge.books.presentation

import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import lucio.maciel.challenge.base.UiModelMapper
import lucio.maciel.challenge.books.infrastructure.model.Book
import lucio.maciel.challenge.books.presentation.model.BookUiModel

class BookUiModelMapper : UiModelMapper<Book, BookUiModel> {
    override fun map(input: Book): BookUiModel {
        val author = input.author?.let {
            buildSpannedString {
                bold { append("Author: ") }
                append(it)
            }
        }
        return BookUiModel(input.title, input.thumbnail, author)
    }

}