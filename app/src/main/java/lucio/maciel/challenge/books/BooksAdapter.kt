package lucio.maciel.challenge.books

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_book.view.*
import lucio.maciel.challenge.R
import lucio.maciel.challenge.books.presentation.model.BookUiModel

class BooksAdapter : RecyclerView.Adapter<BookViewHolder>() {

    private val items = mutableListOf<BookUiModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val view = parent.inflate(R.layout.item_book)
        return BookViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun add(books: List<BookUiModel>) {
        items.addAll(books)
        notifyDataSetChanged()
    }

}

class BookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(book: BookUiModel) {
        with(itemView) {
            title.text = book.title
            if (book.author != null) {
                author.isVisible = true
                author.text = book.author
            } else {
                author.isVisible = false
            }
            thumb.setImageUrl(book.thumbnail)
        }
    }
}

fun ViewGroup.inflate(resId: Int, attachToParent: Boolean = false): View {
    return LayoutInflater.from(context).inflate(resId, this, attachToParent)
}

fun ImageView.setImageUrl(url: String?) {
    Picasso.get().load(url).placeholder(R.color.loadingColor).into(this)
}