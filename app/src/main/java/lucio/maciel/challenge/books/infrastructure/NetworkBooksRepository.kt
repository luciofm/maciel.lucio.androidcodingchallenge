package lucio.maciel.challenge.books.infrastructure

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.withContext
import lucio.maciel.challenge.books.infrastructure.model.Book
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class NetworkBooksRepository : BooksRepository {

    override suspend fun loadBooks(): List<Book> = withContext(CommonPool) {
        val url = URL("http://de-coding-test.s3.amazonaws.com/books.json")
        var connection: HttpURLConnection? = null
        try {
            connection = url.openConnection() as HttpURLConnection
            connection.readTimeout = 3000
            connection.connectTimeout = 3000
            connection.requestMethod = "GET"
            connection.connect()

            val responseCode = connection.responseCode
            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw IOException("HTTP error code: $responseCode")
            }

            val reader = BufferedReader(InputStreamReader(connection.inputStream))
            val total = StringBuilder()
            reader.use {
                reader.lineSequence().forEach { line ->
                    total.append(line)
                }
            }

            return@withContext mapResponse(total.toString())
        } finally {
            connection?.disconnect()
        }
    }

    private fun mapResponse(response: String): List<Book> {
        val array = JSONArray(response)
        val list = mutableListOf<Book>()

        array.forEach<JSONObject> { jsonObject ->
            val title = jsonObject?.getString("title")
            val author = jsonObject?.getStringOrNull("author")
            val imageUrl = jsonObject?.getStringOrNull("imageURL")

            title?.let {
                list.add(Book(title, author, imageUrl))
            }
        }

        return list
    }
}

fun JSONObject.getStringOrNull(key: String): String? = optString(key, null)

inline fun <reified T> JSONArray.forEach(action: (T?) -> Unit) {
    (0..(length() - 1)).forEach { action(get(it) as? T) }
}