package lucio.maciel.challenge.books.presentation

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import lucio.maciel.challenge.base.UiModelMapper
import lucio.maciel.challenge.books.infrastructure.BooksRepository
import lucio.maciel.challenge.books.infrastructure.model.Book
import lucio.maciel.challenge.books.presentation.model.BookUiModel
import kotlin.coroutines.experimental.CoroutineContext

class BooksPresenter(
    private val repository: BooksRepository,
    private val mapper: UiModelMapper<Book, BookUiModel>,
    private val presentationContext: CoroutineContext
) {
    private var view: BooksView? = null
    private var job: Job? = null

    fun attach(view: BooksView) {
        this.view = view
    }

    fun detach() {
        job?.cancel()
        view = null
    }

    fun loadBooks() {
        job?.cancel()
        job = launch(presentationContext) {
            try {
                view?.showLoading()
                val books = repository.loadBooks().map { mapper.map(it) }
                view?.showBooks(books)
            } catch (ex: Exception) {
                ex.printStackTrace()
                view?.showError()
            } finally {
                view?.hideLoading()
            }
        }
    }
}