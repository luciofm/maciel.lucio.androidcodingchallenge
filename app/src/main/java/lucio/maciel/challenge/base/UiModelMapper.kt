package lucio.maciel.challenge.base

interface UiModelMapper<in IN, out OUT> {
    fun map(input: IN): OUT
}